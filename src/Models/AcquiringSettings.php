<?php

namespace Acquiring\Models;

use Illuminate\Database\Eloquent\Model;

class AcquiringSettings extends Model
{
    protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        
        $this->fillable = config('acquiring.fillable.settings');
    }
}
