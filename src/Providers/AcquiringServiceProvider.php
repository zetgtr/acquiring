<?php

namespace Acquiring\Providers;

use Acquiring\View\Admin\Settings;
use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Acquiring\Http\Middleware\Auth as PacageAuth;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class AcquiringServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
        }
        $this->publishes([
            __DIR__.'/../../config/acquiring.php' => config_path('acquiring.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations')
        ], 'migrations');

        $this->app['router']->aliasMiddleware('auth_pacage', PacageAuth::class);


        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'acquiring');


        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->components();
    }

    private function components()
    {
        Blade::component(Settings::class, 'acquiring::admin.settings');
    }

    private function singletons()
    {
    }
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind(QueryBuilder::class, AcquiringBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/acquiring.php', 'acquiring');
        $this->singletons();
    }
}
