<?php

namespace Acquiring\Http\Controllers;

use Acquiring\Models\AcquiringSettings;
use Acquiring\QueryBuilder\PaymentBuilder;
use Acquiring\Request\AlfaBank\CallbackRequest;
use Acquiring\Request\SettingsRequest;
use News\Http\Controllers\Controller;

class AcquiringController extends Controller
{
    public function index(){
        return view('acquiring::acquiring.admin.settings.index');
    }

    public function callback(PaymentBuilder $builder,CallbackRequest $request){
        $builder->callback($request);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(SettingsRequest $request)
    {
        AcquiringSettings::query()->updateOrCreate(['id'=>1],$request->validated());
        return back()->with('success','Настройки успешно сохранены');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
