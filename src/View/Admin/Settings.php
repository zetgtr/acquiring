<?php

namespace Acquiring\View\Admin;

use Acquiring\Enums\BankEnum;
use Closure;
use Acquiring\Models\AcquiringSettings;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Settings extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        $this->acquiring = AcquiringSettings::first();
        $this->banks = BankEnum::all();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('acquiring::components.admin.settings',[
                'acquiring'=> $this->acquiring,
                'banks'=> $this->banks,
            ]);
    }
}
