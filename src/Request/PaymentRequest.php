<?php

namespace Acquiring\Request;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    public function rules(): array
    {
        return config('acquiring.request.paymentRequest');
    }

    public function authorize(): bool
    {
        return true;
    }
}
