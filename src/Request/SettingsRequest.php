<?php

namespace Acquiring\Request;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    public function rules(): array
    {
        return config('acquiring.request.settings');
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'test' => $this->test ? true : false
        ]);
        $this->merge([
            'token_check' => $this->token_check ? true : false
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
