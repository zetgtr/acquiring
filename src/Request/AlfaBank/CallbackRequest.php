<?php

namespace Acquiring\Request\AlfaBank;

use Illuminate\Foundation\Http\FormRequest;

class CallbackRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'orderNumber' => ['nullable','string'],
            'mdOrder' => ['nullable','string'],
            'operation' => ['nullable','string'],
            'status' => ['nullable','integer']
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
