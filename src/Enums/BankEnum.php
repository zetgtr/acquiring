<?php

namespace Acquiring\Enums;

enum BankEnum : string
{
    case ALFA = 'Альфа-банк';
    case KASSA = 'ЮКасса';
    case T_BANK = 'Т-банк';

    public static function all()
    {
        return [
            BankEnum::KASSA,
            BankEnum::ALFA,
            BankEnum::T_BANK,
        ];
    }
}
