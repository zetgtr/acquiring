<?php

namespace Acquiring\Enums;

enum StatusEnum: string
{
    case DEPOSITED = 'deposited';
    case FAILURE = 'failure';
    case PROCESSING = 'processing';
    case APPROVED = 'approved';
    case DECLINED_BY_TIMEOUT = 'declinedByTimeout';
    case REVERSED = 'reversed';
    case REFUNDED = 'refunded';
    case PARICAL_REFUNDED = 'partial_refunded';
    case PARICAL_REVERSED = 'partial_reversed';

    public static function all()
    {
        return [
            StatusEnum::DEPOSITED->value,
            StatusEnum::FAILURE->value,
            StatusEnum::PROCESSING->value,
            StatusEnum::APPROVED->value,
            StatusEnum::DECLINED_BY_TIMEOUT->value,
            StatusEnum::REBERSED->value,
            StatusEnum::REFUNDED->value
        ];
    }

    public static function name($status){
        return match ($status){
            StatusEnum::DEPOSITED->value => 'Оплачен',
            StatusEnum::FAILURE->value => 'Ошибка',
            StatusEnum::PROCESSING->value => 'Создан',
            StatusEnum::APPROVED->value => 'Средства удержаны',
            StatusEnum::DECLINED_BY_TIMEOUT->value => 'Истекло время ожидания',
            StatusEnum::REBERSED->value => 'Отмена',
            StatusEnum::REFUNDED->value => 'Возврат',
        };
    }

    public static function statusColor($status){
        return match ($status){
            StatusEnum::DEPOSITED->value => '#4CAF50', // Оплачено - зелёный
            StatusEnum::FAILURE->value => '#F44336', // Ошибка - красный
            StatusEnum::PROCESSING->value => '#2196F3', // Создан - синий
            StatusEnum::APPROVED->value => '#FF9800', // Средства удержаны - оранжевый
            StatusEnum::DECLINED_BY_TIMEOUT->value => '#9E9E9E', // Истекло время ожидания - серый
            StatusEnum::REBERSED->value => '#FFEB3B', // Отменено - жёлтый
            StatusEnum::REFUNDED->value => '#9C27B0', // Средства возвращены - фиолетовый
        };
    }

    public static function kassa($status){
        return match ($status){
            'payment.waiting_for_capture' => StatusEnum::PROCESSING->value, // Создан
            'payment.succeeded' => StatusEnum::DEPOSITED->value, // Оплачено
            'payment.canceled' => StatusEnum::REBERSED->value, // Отменена
            'refund.succeeded' => StatusEnum::REFUNDED->value, // Возвращено
            'payout.succeeded' => StatusEnum::APPROVED->value, // Средства удержаны
            'payout.canceled' => StatusEnum::REBERSED->value, // Отменена
            'deal.closed' => StatusEnum::APPROVED->value, // Сделка закрыта, средства удержаны
        };
    }
    public static function t_bank($status){
        return match ($status){
            'AUTHORIZED'        => StatusEnum::APPROVED->value,
            'CONFIRMED'         => StatusEnum::DEPOSITED->value,
            'CANCELED'          => StatusEnum::REBERSED->value,
            'REVERSED'          => StatusEnum::REVERSED->value,
            'PARTIAL_REVERSED'  => StatusEnum::PARTIAL_REVERSED->value,
            'REFUNDED'          => StatusEnum::REFUNDED->value,
            'PARTIAL_REFUNDED'  => StatusEnum::PARTIAL_REFUNDED->value,
            'REJECTED'          => StatusEnum::REBERSED->value,
            'DEADLINE_EXPIRED'  => StatusEnum::DECLINED_BY_TIMEOUT->value
        };
    }
}
