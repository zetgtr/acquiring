<?php

namespace Acquiring\QueryBuilder;

use Acquiring\Enums\BankEnum;
use Acquiring\Models\AcquiringSettings;
use Acquiring\Request\AlfaBank\CallbackRequest;
use Acquiring\Request\PaymentRequest;
use Catalog\Models\Category;
use Catalog\Models\Order;
use Catalog\Models\Product;
use Catalog\Models\Settings;
use Document\Models\DocumentCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Request;

class PaymentBuilder
{
    public function __construct()
    {
        $this->settings = AcquiringSettings::first();
        $this->builder = match ($this->settings->bank){
            BankEnum::ALFA->name => new AlfaBuilder(),
            BankEnum::KASSA->name => new KassaBuilder(),
            BankEnum::T_BANK->name => new TBankBuilder(),
            default=> null
        };
    }

    public function getUrl(?Order $order){
        if($this->builder)
            return $this->builder->getPayment($order);
    }

    public function callback(CallbackRequest $request){
        if($this->builder)
            $this->builder->callback($request);
    }
}
