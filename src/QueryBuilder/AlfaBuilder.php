<?php

namespace Acquiring\QueryBuilder;

use Acquiring\Models\AcquiringSettings;
use Acquiring\Request\AlfaBank\CallbackRequest;
use Acquiring\Request\PaymentRequest;
use App\Notifications\OrderFormNotification;
use Catalog\Models\Category;
use Catalog\Models\Order;
use Catalog\Models\Product;
use Catalog\Models\Settings;
use Document\Models\DocumentCategory;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;

class AlfaBuilder
{
    const apiLink = 'https://payment.alfabank.ru/payment';
    const apiLinkTest = 'https://alfa.rbsuat.com/payment';

    private ?Order $order;

    public function __construct()
    {
        $this->settings = AcquiringSettings::first();
    }

    public function getPayment(?Order $order){
        if (class_exists(Order::class)) {
            $this->order = $order;
            if ($this->order) {
                return $this->pay();
            }
        }
        return json_decode(json_encode(['errorCode'=>1,'errorMessage'=>'Ваш заказ не найден! Попробуйте создать заказ заного или оплатить его любым другим способом!']));
    }

    private function pay(){
       $price = $this->order->price;
       if($price){
           if($this->settings->test){
               [$login,$password] = [$this->settings->login_test,$this->settings->password_test];
           }else{
               [$login,$password] = [$this->settings->login,$this->settings->password];
           }
           $testOrder = 0;
           if($this->settings->test)
               $testOrder = time();

           $query = [
               'orderNumber' => $this->order->id ."_".$testOrder,
               'amount' => $price * 100,
               "returnUrl" => env('APP_URL')."/{$this->settings->success_url}",
               "failUrl" => env('APP_URL')."/{$this->settings->failure_url}",
               'email' => $this->order->email
           ];

           if($this->settings->token_check){
               $query['token'] = $this->settings->token;
               if($this->settings->test)
                    $query['token'] = $this->settings->token_test;
           }else{
               $query['userName'] = $login;
               $query['password'] = $password;
           }
           $client = new Client();
           try {
               $link = self::apiLink;
               if($this->settings->test)
                   $link = self::apiLinkTest;


               $response = $client->post($link . '/rest/register.do', [
                   'form_params' => $query,
               ]);

               return json_decode($response->getBody()->getContents());
           } catch (\Exception $e) {
           }

       }else{
           return json_decode(json_encode(['errorCode'=>1,'errorMessage'=>'Не указана цена!']));
       }
    }

    public function callback(CallbackRequest $request){
       $order = Order::find(explode('_',$request->orderNumber)[0]);
       $order->payment_status = $request->operation;
       if($order->save()){
           $method = config('acquiring.callback.method');
           $configModel = config('acquiring.callback.class');
           $model = new $configModel();
           $model->$method($order,$request);
       }
    }
}
