<?php

namespace Acquiring\QueryBuilder;

use Acquiring\Enums\StatusEnum;
use Acquiring\Models\AcquiringSettings;
use Acquiring\Request\AlfaBank\CallbackRequest;
use Acquiring\Request\PaymentRequest;
use App\Notifications\OrderFormNotification;
use Catalog\Enums\PromocodeEnum;
use Catalog\Models\Category;
use Catalog\Models\Order;
use Catalog\Models\Product;
use Catalog\Models\Promocode;
use Catalog\Models\Settings;
use Document\Models\DocumentCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;
use YooKassa\Client;
use Illuminate\Support\Facades\Log;


class KassaBuilder
{

    private ?Order $order;

    public function __construct()
    {
        $this->settings = AcquiringSettings::first();

        $this->setClient();
    }

    private function setClient(){
        if($this->settings->test){
            [$login,$password] = [$this->settings->login_test,$this->settings->password_test];
        }else{
            [$login,$password] = [$this->settings->login,$this->settings->password];
        }
        $this->client = new Client();
        $this->client->setAuth($login, $password);
    }

    public function getPayment(?Order $order){
        if (class_exists(Order::class)) {
            $this->order = $order;
            if ($this->order) {
                return $this->pay();
            }
        }
        return json_decode(json_encode(['errorCode'=>1,'errorMessage'=>'Ваш заказ не найден! Попробуйте создать заказ заного или оплатить его любым другим способом!']));
    }

    private function setPromo($price,$promocode, $count = 1){
        if (!$promocode) {
            return $price;
        }
        $sale = $promocode->sale / $count;

        return match ($promocode->type){
            PromocodeEnum::PRICE->value => $price - $sale,
            PromocodeEnum::PROCENT->value => $price - ($price / 100 * $promocode->sale),
        };
    }
    private function pay(){
        $price = $this->order->price;
        if($price){
            $idempotenceKey = uniqid('', true);

            $priceBonus = $this->order->bonus_minus;
            $total = 0;
            $promocode = Promocode::where('promocode',$this->order->promocode)->first();
            $items = $this->order->products->map(function (Product $product) use (&$priceBonus,&$total, $promocode) {
                $priceProduct = $product->pivot->price;
                if($priceBonus > 0) {
                    $priceProduct = $priceProduct - $sale;
                    $priceBonus = $priceBonus - $product->pivot->price;
                    if($priceProduct < 0){
                        $priceProduct = 0;
                    }
                }
                $total += $priceProduct;

                return [
                    'description' => $product->title,
                    'quantity' => $product->pivot->count.'.00',
                    'amount' => [
                        "value" => $this->setPromo($priceProduct, $promocode, $this->order->products->count()).'.00',
                        "currency" => "RUB"
                    ],
                    "vat_code" => "2",
                    "payment_mode" => "full_prepayment",
                    "payment_subject" => "commodity"
                ];
            })->toArray();
            Log::info($total);
            $response = $this->client->createPayment(
                array(
                    'amount' => array(
                        'value' => $this->setPromo($total, $promocode).'.00',
                        'currency' => 'RUB',
                    ),
                    'confirmation' => array(
                        'type' => 'redirect',
                        'locale' => 'ru_RU',
                        'return_url' => env('APP_URL')."/{$this->settings->success_url}?order={$this->order->id}",
                    ),
                    'receipt' => [
                        'customer' => [
                            'full_name' => $this->order->first_name ? ($this->order->first_name .' '. $this->order->last_name) : $this->order->name,
                            'phone' => $this->order->phone
                        ],
                        'items' => $items
                    ],
                    'capture' => true,
                    'metadata' => array(
                        'orderNumber' => $this->order->id
                    )
                ),
                $idempotenceKey
            );

            return $response->getConfirmation()->getConfirmationUrl();
        }else{
            return json_decode(json_encode(['errorCode'=>1,'errorMessage'=>'Не указана цена!']));
        }
     }

    public function callback(CallbackRequest $request){
        $json_Request = file_get_contents('php://input');
        if(env('APP_DEBUG'))
            \Log::info($json_Request);
        $Request = json_decode($json_Request, true);
        $payment = $this->client->getPaymentInfo($Request['object']['id']);
        $order = Order::find($payment->getMetadata()->unknownProperties['orderNumber']);
        $order->payment_status = StatusEnum::kassa($Request['event']);
        if($order->save()){
            $method = config('acquiring.callback.method');
            $configModel = config('acquiring.callback.class');
            $model = new $configModel();
            $model->$method($order,$payment);
        }
    }
}
