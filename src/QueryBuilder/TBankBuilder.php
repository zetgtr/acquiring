<?php

namespace Acquiring\QueryBuilder;

use Acquiring\Enums\StatusEnum;
use Acquiring\Models\AcquiringSettings;
use Acquiring\Request\AlfaBank\CallbackRequest;
use Acquiring\Request\PaymentRequest;
use App\Notifications\OrderFormNotification;
use Catalog\Enums\PromocodeEnum;
use Catalog\Models\Category;
use Catalog\Models\Order;
use Catalog\Models\Product;
use Catalog\Models\Promocode;
use Catalog\Models\Settings;
use Document\Models\DocumentCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Log;


class TBankBuilder
{

    private ?Order $order;
    private string $terminalKey;
    private string $password;

    public function __construct()
    {
        $this->settings = AcquiringSettings::first();
        $this->setClient();
    }

    private function setClient(){
        if($this->settings->test){
            [$this->terminalKey, $this->password] = [$this->settings->login_test,$this->settings->password_test];
        }else{
            [$this->terminalKey, $this->password] = [$this->settings->login,$this->settings->password];
        }
    }

    public function getPayment(?Order $order){
        if (class_exists(Order::class)) {
            $this->order = $order;
            if ($this->order) {
                return $this->pay();
            }
        }
        return json_decode(json_encode(['errorCode'=>1,'errorMessage'=>'Ваш заказ не найден! Попробуйте создать заказ заного или оплатить его любым другим способом!']));
    }
    private function generateTinkoffToken($data)
    {
        unset($data['Token']);
        unset($data['TerminalKey']);
        unset($data['Receipt']);
        ksort($data);

        $data['Password'] = $this->password;
        $data['TerminalKey'] = $this->terminalKey;
        Log::info($data);
        $tokenString = implode('', array_values($data));
        Log::info($tokenString);
        return hash('sha256', $tokenString);
    }
    private function pay()
    {
        $amount = (int) ($order->total_price * 100);
        $terminalKey = $this->terminalKey;
        $items = [];
        $method = config('acquiring.callback.method_pay');
        $configModel = config('acquiring.callback.class');
        $model = new $configModel();
        $dataPay = compact('amount', 'terminalKey');
        $data = $model->$method($order,$dataPay);
        $dataForToken = $data;
        unset($dataForToken['NotificationURL'], $dataForToken['SuccessURL'], $dataForToken['FailURL']);
        // Генерируем токен
        $data['Token'] = $this->generateTinkoffToken($dataForToken);


        Log::info('Tinkoff Request:', $data);
        $response = Http::post('https://securepay.tinkoff.ru/v2/Init', $data);

        if ($response->successful()) {
            $result = $response->json();

            $order->payment_id = $result['PaymentId'] ?? null;
            $order->status = 'pending';
            $order->save();

            Log::info('Tinkoff Response:', $result);
            return $result['PaymentURL'];
        } else {
            Log::error('Ошибка оплаты Tinkoff:', $response->json());
            return back()->with('error', 'Ошибка оплаты');
        }
    }


    private function generateTinkoffTokenCallback(array $data): string
    {
        $dataString =
            $data['Amount'] .
            $data['CardId'] .
            $data['ErrorCode'].
            $data['ExpDate'] .
            $data['OrderId'] .
            $data['Pan'] .
            $this->password .
            $data['PaymentId'] .
            $data['Status'] .
            ($data['Success'] ? 'true' : 'false') .
            $data['TerminalKey'];

        Log::info('Строка перед хешированием: ' . $dataString);

        return hash('sha256', $dataString);
    }

    public function callback(CallbackRequest $request){
        $json_Request = file_get_contents('php://input');
        if(env('APP_DEBUG'))
            \Log::info($json_Request);
        $Request = json_decode($json_Request, true);
        if (!is_array($data)) {
            Log::error('Ошибка декодирования JSON');
            return response()->json(['message' => 'Invalid JSON'], 400);
        }

// Проверяем, есть ли Token в запросе
        $receivedToken = $data['Token'] ?? null;
        if (!$receivedToken) {
            Log::error('Отсутствует токен в запросе');
            return response()->json(['message' => 'Missing Token'], 400);
        }


        // Убираем только Token перед вычислением подписи
        $dataForToken = $data;
        unset($dataForToken['Token']);

        // Генерируем подпись
        $calculatedToken = $this->generateTinkoffTokenCallback($dataForToken);
        // Проверяем подпись
        if ($receivedToken !== $calculatedToken) {
            Log::error('Ошибка верификации токена', ['received' => $receivedToken, 'calculated' => $calculatedToken]);
            return response()->json(['message' => 'Invalid signature'], 400);
        }

        // Проверяем, передан ли OrderId
        if (!isset($data['OrderId'])) {
            Log::error('Отсутствует OrderId в запросе');
            return response()->json(['message' => 'Missing OrderId'], 400);
        }

        // Ищем заказ в базе
        $order = Order::where('id', (int) $data['OrderId'])->first();
        $statusMap = StatusEnum::t_bank($data['Status']);
        if ($statusMap) {
            $order->status = $statusMap;
            if($order->save()){
                $method = config('acquiring.callback.method');
                $configModel = config('acquiring.callback.class');
                $model = new $configModel();
                return $model->$method($order,$Request);
            }
        } else {
            Log::warning("Неизвестный статус платежа: {$data['Status']} для заказа {$order->id}");
        }

    }
}
