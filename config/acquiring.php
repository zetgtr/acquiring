<?php

use Laravel\Fortify\Features;

return [
    'guard' => 'web',
    'middleware' => ['web'],
    'auth_middleware' => 'auth',
    'passwords' => 'users',
    'username' => 'email',
    'email' => 'email',
    'views' => true,
    'home' => '/home',
    'prefix' => '',
    'domain' => null,
    'limiters' => [
        'login' => null,
    ],
    'redirects' => [
        'login' => null,
        'logout' => null,
        'password-confirmation' => null,
        'register' => null,
        'email-verification' => null,
        'password-reset' => null,
    ],
    'callback' => [
        'class' => 'App\QueryBuilder\Catalog\OrderBuilder',
        'method' => 'runCallback',
    ],
    'fillable' => [
        'settings' => [
            'login_test',
            'password_test',
            'login',
            'password',
            'secure_key',
            'test',
            'token_check',
            'token',
            'token_test',
            'bank',
            'success_url',
            'failure_url',
        ]
    ],
    'request' => [
        'paymentRequest' => [
            'order' => 'required'
        ],
        'settings' => [
            'login_test'=>'nullable',
            'password_test'=>'nullable',
            'login'=>'nullable',
            'password'=>'nullable',
            'secure_key' => 'nullable',
            'test'=>'nullable',
            'bank'=>'required',
            'token_check' => 'nullable',
            'token' => 'nullable',
            'token_test' => 'nullable',
            'success_url'=>'required',
            'failure_url'=>'required'
        ]
    ],
    'features' => [
        Features::registration(),
        Features::resetPasswords(),
        Features::emailVerification(),
        Features::updateProfileInformation(),
        Features::updatePasswords(),
        Features::twoFactorAuthentication(),
    ],
];
