@extends('layouts.admin')
@section('title',"Эквайринг")
@section('content')
    <div class="card">
        <div class="card-body">
            <x-warning />
            <x-acquiring::admin.settings />
        </div>
    </div>
@endsection
