<form action="{{ route('admin.acquiring.store') }}" method="POST" class="row">
    @csrf
    <div class="login_box col-lg-12 mb-3 @if($acquiring && $acquiring->token_check) d-none @endif">
        <div class="row">
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="login_test">Тестовый логин</label>
                    <input type="text" class="form-control" name="login_test" value="{{ old('login_test',$acquiring ? $acquiring->login_test : "") }}">
                    <x-error error-value="login_test" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="password_test">Тестовый пароль</label>
                    <input type="text" class="form-control" name="password_test" value="{{ old('password_test',$acquiring ? $acquiring->password_test : "") }}">
                    <x-error error-value="login_test" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="login">Логин</label>
                    <input type="text" class="form-control" name="login" value="{{ old('login',$acquiring ? $acquiring->login : "") }}">
                    <x-error error-value="login" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="password">Пароль</label>
                    <input type="text" class="form-control" name="password" value="{{ old('password',$acquiring ? $acquiring->password : "") }}">
                    <x-error error-value="password" />
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12 mb-3 token_box @if($acquiring && !$acquiring->token_check) d-none @endif">
        <div class="row">
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="token_test">Тестовый токен доступа</label>
                    <input type="text" class="form-control" name="token_test" value="{{ old('token_test',$acquiring ? $acquiring->token_test : "") }}">
                    <x-error error-value="token_test" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="token">Токен доступа</label>
                    <input type="text" class="form-control" name="token" value="{{ old('token',$acquiring ? $acquiring->token : "") }}">
                    <x-error error-value="token" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 mb-3">
        <div class="row">
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="bank">Банк</label>
                    <select name="bank" class="form-control @error('bank') is-invalid @enderror form-select">
                        @foreach($banks as $key => $bank)
                            <option @selected(old('bank',$acquiring ? $acquiring->bank : null) === $bank->name) value="{{ $bank->name }}">{{ $bank->value }}</option>
                        @endforeach
                    </select>
                    <x-error error-value="bank" />
                </div>
            </div>
            <div class="col-lg-3 secure_key_container">
                <div class="form_group ">
                    <label for="secure_key">Секретный ключ</label>
                    <input type="text" class="form-control" name="secure_key" value="{{ old('secure_key',$acquiring ? $acquiring->secure_key : "") }}">
                    <x-error error-value="secure_key" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="success_url">Страница успешно</label>
                    <input type="text" class="form-control" name="success_url" value="{{ old('success_url',$acquiring ? $acquiring->success_url : "") }}">
                    <x-error error-value="success_url" />
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form_group">
                    <label for="failure_url">Страница не успешно</label>
                    <input type="text" class="form-control" name="failure_url" value="{{ old('failure_url',$acquiring ? $acquiring->failure_url : "") }}">
                    <x-error error-value="failure_url" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="row">
            <div class="form-group col-md-3">
                <label for="test">Тестовый доступ</label>
                <div>
                    <label class="custom-switch">
                        <input type="checkbox" value="1" name="test" class="custom-switch-input" @checked(old('test',$acquiring ? $acquiring->test : ""))>
                        <span class="custom-switch-indicator"></span>
                    </label>
                </div>
            </div>
            <div class="form-group tokken_swich col-md-3">
                <label for="test">Использовать токен</label>
                <div>
                    <label class="custom-switch">
                        <input type="checkbox" value="1" id="token_check" name="token_check" class="custom-switch-input" @checked(old('token_check',$acquiring ? $acquiring->token_check : ""))>
                        <span class="custom-switch-indicator"></span>
                    </label>
                </div>
            </div>
            <div class="col-lg-12">
                <input type="submit" value="Сохранить" class="btn btn-sm btn-success">
            </div>
        </div>
    </div>
</form>
<script>
    $(document).ready(()=>{
        const login = document.querySelector('.login_box')
        const token = document.querySelector('.token_box')
        const tokenCheck = document.querySelector('#token_check')
        const bank = document.querySelector('select[name=bank]')
        tokenCheck.addEventListener('input',setCheckTocken)
        bank.addEventListener('change',setBank)

        function setCheckTocken(){
            if(tokenCheck.checked){
                login.classList.add('d-none')
                token.classList.remove('d-none')
            }else {
                login.classList.remove('d-none')
                token.classList.add('d-none')
            }
        }

        function setBank(){
            const change = bank.value
            const tokkenSwich = document.querySelector('.tokken_swich')
            const secureKeyContainer = document.querySelector('.secure_key_container')
            switch (change) {
                case 'KASSA':
                    secureKeyContainer.classList.add('d-none')
                    tokkenSwich.classList.add('d-none')
                    login.classList.remove('d-none')
                    token.classList.add('d-none')
                    break;
                default:
                    secureKeyContainer.classList.remove('d-none')
                    tokkenSwich.classList.remove('d-none')
                    setCheckTocken()
            }
        }

        setBank()
    })
</script>
