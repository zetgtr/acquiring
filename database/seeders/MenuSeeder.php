<?php

namespace Acquiring\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>997,'name'=>'Эквайринг','position'=>'left','logo'=>'fal fa-sack-dollar','controller'=>'Acquiring\Http\Controllers\AcquiringController','url'=>'acquiring','parent'=>5,"controller_type" => null],
        ];
    }
}
