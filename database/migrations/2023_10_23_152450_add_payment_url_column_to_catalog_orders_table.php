<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Acquiring\Enums\StatusEnum;

return new class extends Migration
{
    public function up()
    {
        Schema::table('catalog_orders', function (Blueprint $table) {
            $table->string('payment_url')->nullable();
            $table->enum('payment_status',StatusEnum::all())->default(StatusEnum::PROCESSING->value);
        });
    }

    public function down()
    {
        Schema::table('catalog_orders', function (Blueprint $table) {
            $table->dropColumn('payment_url');
            $table->dropColumn('payment_status');
        });
    }
};
