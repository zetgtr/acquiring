<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquiringSettingsTable extends Migration
{
    public function up()
    {
        Schema::create('acquiring_settings', function (Blueprint $table) {
            $table->id();
            $table->string('login_test')->nullable();
            $table->string('password_test')->nullable();
            $table->string('login')->nullable();
            $table->string('password')->nullable();
            $table->string('secure_key')->nullable();
            $table->boolean('test')->default(true);
            $table->string('bank');
            $table->string('success_url');
            $table->string('failure_url');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('acquiring_settings');
    }
}
