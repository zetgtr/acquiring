<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::table('acquiring_settings', function (Blueprint $table) {
            $table->string('token')->nullable();
            $table->string('token_test')->nullable();
            $table->boolean('token_check')->default(false);
        });
    }

    public function down()
    {
        Schema::table('acquiring_settings', function (Blueprint $table) {
            $table->dropColumn('token');
            $table->dropColumn('token_test');
            $table->dropColumn('token_check');
        });
    }
};
