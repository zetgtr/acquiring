<?php

use Acquiring\Http\Controllers\AcquiringController;
use Illuminate\Support\Facades\Route;

Route::middleware('api')->group(function (){
    Route::group(['prefix'=>"api"],static function(){
        Route::any('acquiring/callback',[AcquiringController::class,'callback']);
    });
});
